<?php

namespace App\Http\Controllers;

use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public $ratios = [
        'e1' => 2,
        'e2' => 1,
        'e3' => 3,
        'e4' => 2,
        'e5' => 5,
        'e6' => 2,
        'e7' => 1,
        'e8' => 12,
    ];


    public function index()
    {
        $scoresPatch = storage_path("app/scores/scores.xlsx");

        $scores = Excel::toArray(new UsersImport, $scoresPatch);

        $lastScores = Arr::first($scores);

        $this->average($lastScores);

        $pathToFile = storage_path("app/scores/Average.txt");
        return response()->download($pathToFile)->deleteFileAfterSend();
    }


    public function average(array $students)
    {
        $sum = [];
        $result = "";
        foreach ($students as $student) {
            $name = array_shift($student);
            foreach ($student as $ratios => $value) {
                $sum[$ratios] = $value * $this->ratios[$ratios];
            }
            $sums = round(array_sum($sum)/array_sum($this->ratios));
            $result .= $name." Average = ".$sums."\r\n";
        }
        $this->createTextFile($result);

    }


    public function createTextFile(string $content)
    {
        Storage::disk('scores')->put('Average.txt', $content);
    }
}
